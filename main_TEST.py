import unittest
from main import Customer 
from main import Purchase

#Creates a Customer object and two Purchase objects.
#Assigns the Purchase objects to the Customer object.
#Calls the function to sum the AmountPaid by the customer.
#Verifies that the result returned from the AmountPaid sum is correct.

class total_TEST(unittest.TestCase):
    def test_totalAmount(self):
        c1 = Customer(42, "Joe", "Bloggs")
        p1 = Purchase(c1, 1, 10)
        p2 = Purchase(c1, 2, 5)

        totalPaid = Customer.totalSpend(c1, [p1, p2])

        self.assertEqual(15, totalPaid) 

        
if __name__ == "__main__":
    unittest.main()
