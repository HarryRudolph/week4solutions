import csv

customerDataPath = "data/Customers.csv"
purchaseDataPath = "data/Purchases.csv"

customersList = []

class Customer:
    def __init__(self, id, firstname, surname):
        self.id = id
        self.firstname = firstname
        self.surname = surname

    def __repr__(self):
        return (" ID: " + str(self.id) + " First: " + self.firstname + " Last: " + self.surname + ",\n")

    """
    Takes an instance of customer object and returns a list of that customer's purchases. 
    """
    @staticmethod
    def getPurchases(self, purchaseList):
        customerPurchaseList = []

        for row in purchaseList:
            if self.id == row.customer.id:
                customerPurchaseList.append(row)

        return (customerPurchaseList)

    """
    Takes an instance of customer object returns the total amount that the cusotmer has spent as a floating point number.
    """
    @staticmethod    
    def totalSpend(self, purchaseList):
        list = Customer.getPurchases(self, purchaseList)
        total = 0
        for i in list:
            total = total + i.amountPaid

        return total 
    
class Purchase:
    def __init__(self, customer, itemId, amountPaid):
        self.customer = customer
        self.itemId = itemId
        self.amountPaid = amountPaid

    def __repr__(self):
        return (" customerID: " + str(self.customer) + " itemId: " + str(self.itemId) + " Amount: " + str(self.amountPaid) + ",\n")


"""
Function takes a filepath to a formatted CSV file as well as a dictionary to add to. This dictionary is then returned. 
"""
def csvToList(filepath, list, objectType):
    dataFile = open(filepath)
    reader = csv.reader(dataFile, delimiter=',', quotechar="'", quoting = csv.QUOTE_NONNUMERIC)

    i = 0 #Ignore the first title row
    for row in reader:
        if i != 0:
            if objectType == Customer: #If statements as purchase contains customer objects
                list.append(objectType(row[0], row[1], row[2]))
            if objectType == Purchase:
                list.append(objectType(customersList[int(row[0])], row[1], row[2]))
        i = i + 1
    dataFile.close()
   
    
if __name__ == "__main__":

    purchasesList = []
    #Note: The customersList must be created before the purchasesList a purchaseList relies on customersList to be able to create objects. i.e every purchase has to be tied to a customer.
    csvToList(customerDataPath, customersList, Customer)
    csvToList(purchaseDataPath, purchasesList, Purchase)

    print("Printing Customer Objects:\n" + repr(customersList) + "\n")
    print("Printing Purchase Objects:\n" + repr(purchasesList) + "\n")
    print("Printing Purchases for customerID 1:\n" + repr(Customer.getPurchases(customersList[1],purchasesList)) + "\n")
    print("Printing totalSpend for customerID 1:\n" + str(Customer.totalSpend(customersList[1],purchasesList)) + "\n")
